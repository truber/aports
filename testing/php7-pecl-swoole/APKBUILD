# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php7-pecl-swoole
_pkgreal=swoole
pkgver=4.5.8
pkgrel=0
pkgdesc="Event-driven asynchronous and concurrent networking engine with high performance for PHP."
url="https://pecl.php.net/package/swoole"
arch="all"
license="Apache-2.0"
depends="php7-json php7-mysqlnd php7-openssl php7-sockets"
makedepends="php7-dev openssl-dev nghttp2-dev libucontext-dev"
source="https://pecl.php.net/get/$_pkgreal-$pkgver.tgz"
builddir="$srcdir"/$_pkgreal-$pkgver
subpackages="$pkgname-dev"

build() {
	case "$CARCH" in
		ppc64le|s390x|x86) export LDFLAGS="$LDFLAGS -lucontext" ;;
	esac
	phpize7
	./configure --prefix=/usr \
		--with-php-config=php-config7 \
		--enable-mysqlnd \
		--enable-openssl --with-openssl-dir=/usr \
		--enable-sockets \
		--enable-swoole-json \
		--enable-http2
	make
}

check() {
	# needs extra services to test all suite
	php7 -d extension=modules/swoole.so --ri swoole
}

package() {
	make INSTALL_ROOT="$pkgdir"/ install
	local _confdir="$pkgdir"/etc/php7/conf.d
	install -d $_confdir
	echo "extension=$_pkgreal.so" > $_confdir/50_$_pkgreal.ini
}

sha512sums="1ed5ea36cf9f0cdedc071424571a0dc1c13e303c0e88be1a762945d8f9b57b68b43b213f5e7041d6a046023cf8a15bdfbb6e9bf6632580936dbf0920d5422c42  swoole-4.5.8.tgz"
